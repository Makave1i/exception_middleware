import traceback
from django.utils.deprecation import MiddlewareMixin
from app.tasks import save_exception


class CustomExceptionMiddleware(MiddlewareMixin):
    @staticmethod
    def process_exception(_request, exception):
        trace_back = traceback.format_exc()

        json_exception = dict(
            type=exception.__class__.__name__,
            string=str(exception),
            repr=repr(exception),
            args=', '.join(exception.args),
            traceback=str(trace_back)
        )

        save_exception.delay(json_exception)

        return None
