from django.urls import path
from .views import RaiseExceptionView, ExceptionListView


app_name = 'app'

urlpatterns = [
    path('', ExceptionListView.as_view(), name='list'),
    path('<str:param>', RaiseExceptionView.as_view(), name='error'),
]
