import os
import json

from django.test import TestCase
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.urls import reverse

from rest_framework.test import APIClient

from .middleware import CustomExceptionMiddleware
from .tasks import save_exception


# Create your tests here.
class MiddlewareTests(TestCase):
    def test_middleware_response(self):
        middleware = CustomExceptionMiddleware()
        exceptions_message = 'test'
        exception = ObjectDoesNotExist(exceptions_message)
        res = middleware.process_exception(None, exception)
        self.assertIsNone(res)


class CustomLoggerTests(TestCase):

    def setUp(self):
        self.old_setting = settings.LOG_FILE
        settings.LOG_FILE = os.path.join(settings.BASE_DIR, 'logtest.pickle')
        self.factory = APIClient()

    def tearDown(self):
        if os.path.exists(settings.LOG_FILE):
            os.remove(settings.LOG_FILE)

    def test_save_exception(self):
        self.assertFalse(os.path.exists(settings.LOG_FILE))
        exception = dict(test='message')
        save_exception(exception)
        self.assertTrue(os.path.exists(settings.LOG_FILE))

    def test_exception_view(self):
        request = self.factory.get(reverse('main:error', kwargs={'param': 'test'}))
        self.assertEqual(request.status_code, 400)

    def test_list_view(self):
        request = self.factory.get(reverse('main:list'))
        self.assertEqual(request.status_code, 200)
        data = json.loads(request.content)
        self.assertListEqual(data["results"], [])
        save_exception(dict(test='message'))
        request = self.factory.get(reverse('main:list'))
        data = json.loads(request.content)
        self.assertEqual(len(data["results"]), 1)
