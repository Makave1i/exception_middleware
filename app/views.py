import os
import pickle
from random import choice

from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned, SuspiciousOperation, DisallowedHost
from django.conf import settings

from rest_framework.views import APIView
from rest_framework.response import Response

EXCEPTIONS = [ObjectDoesNotExist, MultipleObjectsReturned, SuspiciousOperation, DisallowedHost]


class RaiseExceptionView(APIView):
    @staticmethod
    def get(request, param):
        exception = choice(EXCEPTIONS)
        if exception:
            raise exception(f'error = {param}')

        return Response({'error': param})


class ExceptionListView(APIView):
    @staticmethod
    def get(request):
        objects = []
        if os.path.exists(settings.LOG_FILE):
            with (open(settings.LOG_FILE, "rb")) as openfile:
                while True:
                    try:
                        objects.append(pickle.load(openfile))
                    except EOFError:
                        break

        return Response({'success': True, 'results': objects})
