import pickle
from celery import shared_task
from django.conf import settings

@shared_task
def save_exception(exception):
    with open(settings.LOG_FILE, 'ab') as f:
        pickle.dump(exception, f)
