import os
import logging
from celery import Celery, signals


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'exception_middleware.settings')

app = Celery('exception_middleware')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

@signals.setup_logging.connect
def setup_celery_logging(**kwargs):
    return logging.getLogger('celery')

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))