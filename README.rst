Installing
----------
::

    $ cd docker && docker-compose build
::

Running Server
----------
::

    $ docker-compose up
::

URLs
----------

exceptions - http://127.0.0.1:8000/something

logs - http://127.0.0.1:8000/
